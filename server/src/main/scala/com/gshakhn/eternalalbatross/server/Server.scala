package com.gshakhn.eternalalbatross.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scalatags.Text.all.{head, _}
import scalatags.Text.tags2.title

object Server {
  def main(args: Array[String]) {
    implicit val system: ActorSystem = ActorSystem("server-system")
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val route: Route =
      path("") {
        get {
          complete(HttpEntity(
            ContentTypes.`text/html(UTF-8)`,
            "<!DOCTYPE html>" +
              html(
                head(
                  title("Eternal Albatross")
                ),
                body(
                  script(`type` := "text/javascript", src := "/assets/client-jsdeps.js"),
                  script(`type` := "text/javascript", src := "/assets/client-fastopt.js")
                )
              )
          ))
        }
      } ~ pathPrefix("assets" / Remaining) { file =>
        encodeResponse {
          getFromResource("public/" + file)
        }
      }

    Http().bindAndHandle(route, "0.0.0.0", 8080)
  }
}

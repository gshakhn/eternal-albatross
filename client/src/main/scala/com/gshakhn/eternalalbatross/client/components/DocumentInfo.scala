package com.gshakhn.eternalalbatross.client.components

import com.gshakhn.eternalalbatross.client.Document
import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.html_<^._

object DocumentInfo {

  case class Props(document: Document)

  case class Backend(bs: BackendScope[Props, Unit]) {
    def render(p: Props): VdomElement =
      <.div(s"Document Name: ${p.document.title}",
        s"Page count: ${p.document.pages.size}")
  }

  private val component =
    ScalaComponent.builder[Props]("DocumentInfo")
      .renderBackend[Backend]
      .build

  def apply(document: Document): TagMod = {
    component(Props(document))
  }

}

package com.gshakhn.eternalalbatross.client

import com.gshakhn.eternalalbatross.client.Page.FileData
import diode.ActionResult.ModelUpdate
import diode._
import diode.react.ReactConnector

object AppCircuit extends Circuit[AppModel] with ReactConnector[AppModel] {
  override protected def initialModel = AppModel(
    Seq(Document("hello", Seq.empty),
      Document("world", Seq.empty)),
    ViewingDocuments,
    EditableDocument(Document("New title", Seq.empty)))

  override protected def actionHandler: AppCircuit.HandlerFunction = {
    composeHandlers(
      new ViewHandler(zoomRW(a => a)((a, d) => d)),
      new EditDocumentHandler(zoomTo(_.editableDocument)))
  }
}

case object Init extends Action

trait ViewAction extends Action

case object ViewDocumentsA extends ViewAction

case object NewDocumentA extends ViewAction

class ViewHandler[M](modelRW: ModelRW[M, AppModel]) extends ActionHandler(modelRW) {
  override protected def handle: PartialFunction[Any, ActionResult[M]] = {
    case ViewDocumentsA => ModelUpdate(modelRW.zoomTo(_.view).updated(ViewingDocuments))
    case NewDocumentA =>
      ModelUpdate(modelRW.zoomTo(_.view).updated(EditingDocument))
  }
}


trait EditingDocumentA extends Action

case class AddPageA(data: FileData) extends EditingDocumentA

case class InputTextFieldChangeA(newValue: String) extends Action

class EditDocumentHandler[M](modelRW: ModelRW[M, EditableDocument]) extends ActionHandler(modelRW) {
  override protected def handle: PartialFunction[Any, ActionResult[M]] = {
    case InputTextFieldChangeA(newValue) => ModelUpdate(modelRW.zoomTo(_.document.title).updated(newValue))
  }
}
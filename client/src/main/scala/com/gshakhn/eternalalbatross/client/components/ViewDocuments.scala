package com.gshakhn.eternalalbatross.client.components

import com.gshakhn.eternalalbatross.client.Document
import diode.react.ModelProxy
import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.component.Scala.{BackendScope, Unmounted}
import japgolly.scalajs.react.vdom.Implicits._
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.html_<^.<

object ViewDocuments {

  case class Props(proxy: ModelProxy[Seq[Document]])

  class Backend(bs: BackendScope[Props, Unit]) {
    def render(p: Props): VdomElement = {
      <.div(p.proxy().toTagMod(DocumentInfo(_)))
    }
  }

  private val component =
    ScalaComponent.builder[Props]("ViewDocuments")
      .renderBackend[Backend]
      .build

  def apply(proxy: ModelProxy[Seq[Document]]): Unmounted[Props, Unit, Backend] =
    component(Props(proxy))
}
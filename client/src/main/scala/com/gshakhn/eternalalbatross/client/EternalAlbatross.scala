package com.gshakhn.eternalalbatross.client

import com.gshakhn.eternalalbatross.client.components.Root
import japgolly.scalajs.react.vdom.Implicits._
import org.scalajs.dom.document

object EternalAlbatross {
  def main(args: Array[String]): Unit = {
    val rootConnection = AppCircuit.connect { a => a }

    val rootDiv = document.createElement("div")
    rootConnection(p => Root(p)).renderIntoDOM(rootDiv)
    document.body.appendChild(rootDiv)

  }
}

package com.gshakhn.eternalalbatross.client.components

import com.gshakhn.eternalalbatross.client._
import diode.react.ModelProxy
import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.Unmounted
import japgolly.scalajs.react.vdom.html_<^._

object Root {

  case class Props(proxy: ModelProxy[AppModel])

  class Backend(bs: BackendScope[Props, Unit]) {
    def render(p: Props): VdomElement = {
      <.div(
        <.button("View Documents", ^.onClick --> p.proxy.dispatchCB(ViewDocumentsA)),
        <.button("Upload New Document", ^.onClick --> p.proxy.dispatchCB(NewDocumentA)),
        p.proxy().view match {
          case ViewingDocuments => ViewDocuments(p.proxy.zoom(_.documents))
          case EditingDocument => EditDocument(p.proxy.zoom(_.editableDocument))
        }
      )
    }
  }

  private val component =
    ScalaComponent.builder[Props]("Root")
      .renderBackend[Backend]
      .build

  def apply(proxy: ModelProxy[AppModel]): Unmounted[Props, Unit, Backend] = {
    component(Props(proxy))
  }
}

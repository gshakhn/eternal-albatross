package com.gshakhn.eternalalbatross.client

import com.gshakhn.eternalalbatross.client.Page.FileData

case class AppModel(documents: Seq[Document],
                    view: ViewState,
                    editableDocument: EditableDocument)


case class Document(title: String, pages: Seq[Page])

object Page {
  type FileData = Seq[Byte]
}

case class Page(data: FileData)


trait ViewState

case object ViewingDocuments extends ViewState

case object EditingDocument extends ViewState

case class EditableDocument(document: Document)
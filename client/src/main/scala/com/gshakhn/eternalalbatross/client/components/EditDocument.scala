package com.gshakhn.eternalalbatross.client.components

import com.gshakhn.eternalalbatross.client.{EditableDocument, InputTextFieldChangeA}
import diode.react.ModelProxy
import japgolly.scalajs.react.{ReactEventFromInput, ScalaComponent}
import japgolly.scalajs.react.component.Scala.{BackendScope, Unmounted}
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.html_<^.{<, _}

object EditDocument {

  case class Props(proxy: ModelProxy[EditableDocument])

  class Backend(bs: BackendScope[Props, Unit]) {
    def render(p: Props): VdomElement = {
      <.div("editing document",
        <.input(
          ^.`type` := "text",
          ^.value := p.proxy().document.title,
          ^.onChange ==> {
            e: ReactEventFromInput => {
              p.proxy.zoom(_.document.title)
              p.proxy.dispatchCB(InputTextFieldChangeA(e.target.value))
            }
          }))
    }
  }

  private val component =
    ScalaComponent.builder[Props]("EditDocuments")
      .renderBackend[Backend]
      .build

  def apply(proxy: ModelProxy[EditableDocument]): Unmounted[Props, Unit, Backend] =
    component(Props(proxy))

}


scalaVersion in ThisBuild := "2.12.3"

organization in ThisBuild := "com.gshakhn"

val commonSettings = Seq(
  libraryDependencies += "org.scalatest" %%% "scalatest" % "3.0.1" % "test",
  scalacOptions ++= Seq(
    "-feature",
    "-deprecation",
    "-Xlint",
    "-Xfatal-warnings"
  )
)

val shared = crossProject.in(file(".")).settings(commonSettings: _*)

lazy val sharedJVM = shared.jvm

lazy val sharedJS = shared.js

val client = project.dependsOn(sharedJS)
  .settings(commonSettings: _*)
  .enablePlugins(ScalaJSPlugin)
  .enablePlugins(ScalaJSWeb)
  .settings(
    jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv(),
    scalaJSUseMainModuleInitializer := true,
    libraryDependencies ++= Seq(
      "com.github.japgolly.scalajs-react" %%% "core" % "1.1.1",
      "io.suzaku" %%% "diode" % "1.1.3",
      "io.suzaku" %%% "diode-react" % "1.1.3"),
    jsDependencies ++= Seq(
      "org.webjars.bower" % "react" % "15.6.1"
        / "react-with-addons.js"
        minified "react-with-addons.min.js"
        commonJSName "React",

      "org.webjars.bower" % "react" % "15.6.1"
        / "react-dom.js"
        minified "react-dom.min.js"
        dependsOn "react-with-addons.js"
        commonJSName "ReactDOM"
    )
  )

val server = project.dependsOn(sharedJVM)
  .settings(commonSettings: _*)
  .enablePlugins(SbtWeb)
  .settings(
    scalaJSProjects := Seq(client),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % "10.0.10",
      "com.lihaoyi" %% "scalatags" % "0.6.7",
      "com.vmunier" %% "scalajs-scripts" % "1.1.0"
    ),
    WebKeys.packagePrefix in Assets := "public/",
    managedClasspath in Runtime += (packageBin in Assets).value
  )